package com.ukma.nechyporchuk.network;

public interface Receiver {
    void receiveMessage();
}
